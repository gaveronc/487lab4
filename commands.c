#include "commands.h"

//Turn on some LEDs
void LED_ON (char * LED) {
	//Turn on an LED
	if (LED[0] < '0' || LED[0] > '8' || LED[1] != 0) {
		SendLine("Argument is not an LED\r\n");
		return;
	}
	if (LED[0] == '0') {//Turn on all LEDs
		GPIOB_ODR |= 0xFF00;
	} else {
		GPIOB_ODR |= 1 << (LED[0] - 41);
	}
}

//Turn off some LEDs
void LED_OFF (char * LED) {
	//Turn off an LED
	if (LED[0] < '0' || LED[0] > '8' || LED[1] != 0) {
		SendLine("Argument is not an LED\r\n");
		return;
	}
	if (LED[0] == '0') {
		GPIOB_ODR &= ~(0xFF00);
	} else {
		GPIOB_ODR &= ~(1 << (LED[0]-41));
	}
}

//Describe commands
void HELP (char * data) {
	if(data[0] != 0) {
		SendLine("Ignoring arguments\r\n");
	}
	SendLine("This is the help menu\r\n");
	SendLine("\"LED ON X\" will turn on LED X\r\n");
	SendLine("\"LED OFF X\" will turn off LED X\r\n");
	SendLine("\"LED ON 0\" will turn on all LEDs\r\n");
	SendLine("\"LED OFF 0\" will turn off all LEDs\r\n");
	SendLine("\"QUERY LED X\" will report the status of LED X\r\n");
	SendLine("\"SET ANGLE XYZ\" sets the angle of the valve to the number XYZ\r\n");
	SendLine("\"VALVE ON\" turns valve controls on\r\n");
	SendLine("\"VALVE OFF\" turns valve controls off\r\n");
	SendLine("\"INFO\" prints date and time of compilation\r\n");
	SendLine("\"HELP\" prints this help message\r\n");
}

//Return state of LED
void QUERY_LED (char * LED) {
	if (LED[1] != 0 || LED[0] < '1' || LED[0] > '8') {
		SendLine("Argument is not an LED\r\n");
		return;
	}
	if (GPIOB_IDR & (1 << (LED[0]-41))) {
		SendLine("The LED is on\r\n");
	} else {
		SendLine("The LED is off\r\n");
	}
}

//Set angle of valve on equipment
void SETANGLE (char * data) {
	uint16_t percent = 0;
	int i;
	for (i = 0; i < 3; i += 1) {
		if((data[i] > '9' || data[i] < '0') && data[i] != 0) {
			SendLine("Invalid argument: ");
			SendLine(data);
			SendLine("\r\nExitting command\r\n");
			return;
		}
	}
	
	if(data[2] == 0) {
		if(data[1] == 0) {
			if(data[0] == 0) {
				SendLine("Error: No input value\r\n");
			} else {
				percent = data[0] - 48;			//Only one digit
			}
		} else {//Two digits
			percent = data[1] - 48;
			percent += (data[0] - 48) * 10;
		}
	} else {
		percent = data[2] - 48;					//Ones
		percent += (data[1] - 48) * 10;	//Tens
		percent += (data[0] - 48) * 100;	//Hundreds
	}
	if(percent <= 100)
	{
		PWMAngleSet(percent);
		SendLine("Angle set to ");
		SendInt(percent);
		SendLine("%\r\n");
	} else {
		SendLine("Can't set angle above 100%\r\n");
	}
}

//Enable valve system
void VALVEON (char * data) {
	if(data[0] != 0)
		SendLine("Ignoring arguments\r\n");
	PWMTimerStart();
	SendLine("Timer started\r\n");
}

//Disable valve system
void VALVEOFF (char * data) {
	if(data[0] != 0)
		SendLine("Ignoring arguments\r\n");
	PWMTimerStop();
	SendLine("Timer stopped\r\n");
}

//Display date and time of compilation
void INFO (char * data) {
	if(data[0] != 0) {
		SendLine("Ignoring arguments\r\n");
	}
	SendLine("Compilation time and date:\r\n");
	SendLine(__TIME__);
	SendLine("\r\n");
	SendLine(__DATE__);
	SendLine("\r\n");
}

void ERROR (char * data) {
	SendLine("Error: invalid command ");
	SendLine(data);
	SendLine("\r\n");
}
