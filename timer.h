#ifndef TIMER_H
#define TIMER_H
#include "registers.h"
#include <stdint.h>

void InitTimer(void);
uint16_t TimerStart(void);
uint16_t TimerStop(int16_t start_time);
void TimerShutdown(void);
void InitPWMTimer(void);
void PWMTimerStart(void);
void PWMTimerStop(void);
void PWMAngleSet(uint16_t percent);

#endif
