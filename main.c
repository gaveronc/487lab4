/*
* Cameron Gaveronski
* 200230075
* ENEL 487
* Lab 3
*/

#include <stdint.h>
#include "registers.h"
#include "UART.h" //Enable UART functions
#include "commands.h"
#include "timer.h"

/*
* This program implements a command prompt on the lab board
* that can be accessed over a serial COM port using putty
* or another similar serial terminal program.
* This program maps character arrays to function calls by
* taking all of the input up to a certain point and comparing
* it to an array of strings to try finding a match. When a
* complete match is found, everything following the function
* call is treated as the function's argument. To do this in a
* scalable way, each function, whether it uses arguments or
* not, has to take a pointer to a character array.
* 
* More important than the parsing methods, however, is the
* newly-implemented valve control features. First, the valve
* controls must be turned on with VALVE ON. Then the angle of
* the valve can be set with SET ANGLE <percentage>
*/

//Initialize clocks and GPIO pins
void SysInit(void) {
	//Need to setup the the LED's on the board
	RCC_APB2ENR |= 0x08; // Enable Port B clock
	GPIOB_ODR  &= ~0x0000FF00; // Switch off LEDs
	GPIOB_CRL = 0xB3333333;
	GPIOB_CRH  = 0x33333333; //50MHz  General Purpose output push-pull
}

int main(void)
{
	uint8_t commandByte;
	char command[COMMAND_LENGTH] = {0};
	char data[COMMAND_LENGTH] = {0};
	unsigned charCount = 0;
	unsigned dataNum = 0;
	int i, j, k, c, d;//Count variables
	
	//Initialize clocks and GPIO pins
	SysInit();
	
	//Set up timers
	InitTimer();
	InitPWMTimer();
	
	Serial_Open();//Start serial communications
	
	SendLine(" \n\rSeaBiscuit > ");//Command prompt
	
	while(1) {
		commandByte = GetByte();
		if((commandByte == 0x8) || (commandByte == 0x7F)) {//Check for backspace
			if (charCount > 0) {
				SendByte(0x8);//Mirror the backspace
				SendByte(0x20);//Send a whitespace to clear a character
				SendByte(0x8);//Move back one spot
				
				//Remove the last byte in the command buffer and decrement count
				command[charCount] = 0;
				charCount -= 1;
			}
		} else {
			if (commandByte == 0xD) {//Check for enter key
				SendByte(0xA);//Send carriage return and line feed to the typewriter
				SendByte(0xD);
				
				if(charCount != 0) {
					//Execute command
					
					//First, take out any pairs of whitespaces
					for (c = 0; c < (COMMAND_LENGTH - 1) && command[c] != 0; c += 1) {
						if (command[c] == ' ' && command[c+1] == ' ') {//If 2 whitespaces...
							for (d = c; d < (COMMAND_LENGTH - 1) && command[d] != 0; d += 1) {
								command[d] = command[d+1];//...shift everything after the first space left by one
							}
							c -= 1;//Checks for more than 2 whitespaces
						}
					}
					
					for (i = 0; i < COMMAND_LIST_LENGTH; i += 1) {
						for (j = 0; (j < COMMAND_LENGTH) && ((COMMANDLIST[i][j] == command[j]) || (COMMANDLIST[i][j] == 0x0)); j += 1) {
							//Look for complete match, get any data
							if (COMMANDLIST[i][j] == 0x0) {
								data[dataNum] = command[j];
								dataNum += 1;
							}
						}
						//Everything else is data
						if (j == COMMAND_LENGTH) {
							functionCalls[i](data);
							break;
						}
					}
					
					//If nothing found, print an error
					if (i == COMMAND_LIST_LENGTH) {
						ERROR(command);
					}
					
					//Reset character strings
					charCount = 0;
					dataNum = 0;
					for (k = 0; k < COMMAND_LENGTH; k += 1) {
						command[k] = 0;
						data[k] = 0;
					}
				}
				SendLine("SeaBiscuit > ");//Command prompt
			} else {
				if ((charCount < (COMMAND_LENGTH - 1)) && (commandByte >= 0x20)) {
					//Add the character to the command and increment counter, ignoring garbage characters
					//Also, capitalize any lower-case characters
					if ((commandByte >= 'a') && (commandByte <= 'z')) {
						commandByte -= 32;
					}
					command[charCount] = commandByte;
					charCount += 1;
					//Mirror the byte
					SendByte(commandByte);
				}
			}
		}
	}
}
