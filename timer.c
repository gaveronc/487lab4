#include "timer.h"

void InitTimer(void) {
	TIM2_ARR = 0;//Start from 0
	RCC_APB1ENR |= 1;//Enable clock to TIM2
	TIM2_CR1 |= 1 << 4;//Configure as count down (for some reason)
	TIM2_CR1 |= 1;//Enable timer
}

uint16_t TimerStart(void) {
	return TIM2_CNT;//Return count
}

uint16_t TimerStop(int16_t start_time) {
	int time = start_time - TIM2_CNT;
	
	if (time < 0) {//Handle wraparound
		time += 0xFFFF + 1;
	}
	
	return (uint16_t) time;//Return count
}

void TimerShutdown(void) {
	RCC_APB1ENR &= ~1;//Disable clock to TIM2
	TIM2_CR1 &= ~1;//Disable timer
}

void InitPWMTimer(void) {
	RCC_APB1ENR |= 1 << 2;			//Enable clock to TIM4
	TIM4_CCMR1 |= 	6 << 12|//Channel 2 PWM Mode
					1 << 10;//Channel 2 Fast compare enable
	
	TIM4_PSC = 71;				//Divide clock down to 1MHz
	TIM4_ARR = 20000;			//20,000uS (max period)
	TIM4_DIER |= 1;				//Enable update interrupt
	TIM4_CCR2 = 600;			//0 Degrees default
	TIM4_CR1 |= 1;  			//Enable timer
}

void PWMTimerStart(void) {
	TIM4_CCER |= 1 << 4;	//Channel 2 output enable
}

void PWMTimerStop(void) {
	TIM4_CCER &= ~(1 << 4);	//Disable channel 2 output
	GPIOB_ODR &= ~(1 << 6);	//Set output to 0
}

void PWMAngleSet(uint16_t percent) {
	//Put the percentage into the range of 600-2,400uS
	TIM4_CCR2 = (uint16_t)(600 + ((((float)percent)/100)*1800));
}
